// eslint-disable-next-line import/extensions
import { validate } from './email-validator';

const onload = {
  create(type) {
    switch (type) {
      case 'standard':
        return this.registerSection('Join Our Program', 'Subscribe');
      case 'advanced':
        return this.registerSection('Join Our Advanced Program', 'Subscribe to Advanced Program');
      default:
        throw new Error(`Section type ${type} is not supported.`);
    }
  },

  registerSection(message, button_text) {
    const existingSection = document.getElementById('app-sect');

    // create a new section element
    const newSection = document.createElement('section');
    newSection.classList.add('app-section');
    newSection.classList.add('app-join-programme');

    // create a new div element and set its display to flex with column direction
    const flexContainer = document.createElement('div');
    flexContainer.style.display = 'flex';
    flexContainer.style.flexDirection = 'column';
    flexContainer.style.height = '100%';
    flexContainer.style.margin = '100px 0';
    newSection.appendChild(flexContainer);

    // create three new child elements to add to the flex container
    const childElement1 = document.createElement('h2');
    const childElement2 = document.createElement('div');
    const childElement3 = document.createElement('form');

    // add content to each of the child elements
    childElement1.textContent = message;
    childElement2.innerHTML = 'Sed do eiusmod tempor incididunt' + '<br />' + 'ut labore et dolore magna aliqua.';

    // create input field and button elements
    const inputField = document.createElement('input');
    inputField.type = 'text';
    inputField.style.color = 'white';
    inputField.style.width = '400px';
    inputField.style.height = '36px';
    inputField.style.background = 'rgba(255, 255, 255, 0.15)';
    inputField.style.border = '0';

    // placing localStorage (if any) for onload event in main.js ONLOAD
    if (localStorage.getItem('email')) {
      inputField.value = localStorage.getItem('email');
    } else {
      inputField.placeholder = 'email';
    }

    // local storage EVENT update on inputField   INPUT

    inputField.addEventListener('input', (event) => {
      event.preventDefault();
      //if (validate(inputField.value)) {
        localStorage.setItem('email', inputField.value);
      //}
    });

    // button text and style reference

    const button = document.createElement('button');
    button.classList.add('app-section__button--submit', 'app-section__button');

    if (localStorage.getItem('subscribed') === 'true') {
      button.textContent = 'unsubsribe';
      inputField.style.display = 'none'; // no input field if already subscribed
    } else {
      button.textContent = button_text;
    }

    //= ================================================================
    // prevent default and console log  BUTTON CLICK
    //= ================================================================
    button.addEventListener('click', (event) => {
      event.preventDefault();
      if (localStorage.getItem('subscribed') === 'true') {
        localStorage.removeItem('email');
        inputField.style.display = 'flex';
        localStorage.setItem('subscribed', 'false');
        button.textContent = button_text;
        inputField.value = '';
        inputField.placeholder = 'email';
      } else if (validate(inputField.value)) {
        inputField.style.display = 'none';
        button.textContent = 'Unsubscribe';
        localStorage.setItem('subscribed', 'true');
      } else {
        alert('The email address is incorrect');
      }
      // This line uses console.log, which triggers the no-console rule
      /* eslint-disable no-console */
      console.log(inputField.value);
      /* eslint-enable no-console */
    });

    // add the input field and button to the childElement3 div
    childElement3.appendChild(inputField);
    childElement3.appendChild(button);

    childElement1.classList.add('app-title');
    childElement2.classList.add('app-subtitle');

    // set the child elements' heights to 33.33%
    childElement1.style.flex = '1 1 0';
    childElement2.style.flex = '1 1 0';
    childElement3.style.flex = '1 1 0';

    childElement2.style.marginBottom = '20px';

    // add display: flex and justify-content: center to center child elements horizontally
    childElement1.style.display = 'flex';
    childElement1.style.justifyContent = 'center';
    childElement2.style.display = 'flex';
    childElement2.style.justifyContent = 'center';
    childElement3.style.display = 'flex';
    childElement3.style.justifyContent = 'center';

    flexContainer.appendChild(childElement1);
    flexContainer.appendChild(childElement2);
    flexContainer.appendChild(childElement3);

    // insert the new section element after the existing section element
    existingSection.insertAdjacentElement('afterend', newSection);
  },
};

export default onload;
