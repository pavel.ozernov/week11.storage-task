const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const CopyPlugin = require('copy-webpack-plugin');
const ESLintPlugin = require('eslint-webpack-plugin');

const options = {
  // ESLint configuration options
  context: 'src',
  eslintPath: require.resolve('eslint'),
  extensions: ['js', 'jsx'],
  emitError: true,
  emitWarning: true,
  failOnError: true,
  failOnWarning: false,
  fix: true,
};

module.exports = {
  mode: 'development',
  entry: {
    main: path.resolve(__dirname, 'src/main.js'),
  },
  output: {
    path: path.resolve(__dirname, 'dist'),
    filename: '[name].[contenthash].js',
    clean: true,
  },
  devtool: 'inline-source-map',
  devServer: {
    static: path.resolve(__dirname, 'dist'),
    port: 5000,
    open: true,
    hot: true,
  },

  // loaders

  module: {
    rules: [
      {
        test: /\.css$/i,
        use: ['style-loader', 'css-loader'],
      },
      {
        test: /\.m?js$/,
        exclude: /node_modules/,
        use: {
          loader: 'babel-loader',
          options: {
            presets: [
              ['@babel/preset-env', { targets: "defaults" }]
            ]
          }
        }
      }
    ],
  },

  // plugins
  plugins: [
    new HtmlWebpackPlugin({
      template: 'src/index.html',
    }),
    new CopyPlugin({
      patterns: [
        { from: 'src/assets/images/your-logo-footer.png', to: './assets/images/your-logo-footer.png' },
        { from: 'src//assets/images/your-logo-here.png', to: './assets/images/your-logo-here.png' },
      ],
    }),
    new ESLintPlugin(options),
  ],
};
